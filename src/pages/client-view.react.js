// @flow

import * as React from "react";

import {
    Page,
    Avatar,
    Icon,
    Grid,
    Card,
    Text,
    Table,
    Alert,
    Progress,
    colors,
    Dropdown,
    Button,
    StampCard,
    StatsCard,
    ProgressCard,
    Badge,
    Profile,
    Media,
    Form
} from "tabler-react";

import { withStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import PhoneIcon from '@material-ui/icons/Phone';
import FavoriteIcon from '@material-ui/icons/Favorite';
import PersonPinIcon from '@material-ui/icons/PersonPin';
import HelpIcon from '@material-ui/icons/Help';
import ShoppingBasket from '@material-ui/icons/ShoppingBasket';
import ThumbDown from '@material-ui/icons/ThumbDown';
import ThumbUp from '@material-ui/icons/ThumbUp';
import Typography from '@material-ui/core/Typography';

import C3Chart from "react-c3js";

import SiteWrapper from "../SiteWrapper.react";

function ClientView() {


    return (
        <SiteWrapper>
            <Page.Content title="Client Dashboard">
                <Grid.Row>
                    <Grid.Col>
                        <Alert type="secondary">
                            <Alert.Link
                                href={
                                    process.env.NODE_ENV === "production"
                                        ? "https://tabler.github.io/tabler-react/documentation"
                                        : "/documentation"
                                }
                            >
                                Alerts
                         </Alert.Link>{" "}
                        </Alert>
                    </Grid.Col>
                </Grid.Row>
                <Grid.Row cards={true}>

                    <Grid.Col width={6} sm={4} lg={3}>
                        <Grid.Row>
                            <Grid.Col lg={12}>
                                <Profile
                                    name="Peter Richards"
                                    backgroundURL="demo/photos/eberhard-grossgasteiger-311213-500.jpg"
                                    avatarURL="demo/faces/male/16.jpg"
                                    twitterURL="test"
                                >
                                    ABC co. Ltd. CEO
                            <Media>
                                        <Media.BodySocial
                                            name=""
                                            workTitle=""
                                            facebook="Facebook"
                                            twitter="Twitter"
                                            phone="1234567890"
                                            skype="@skypename"
                                        />
                                    </Media>
                                </Profile>
                            </Grid.Col>
                        </Grid.Row>
                        <Grid.Row>
                            <Grid.Col width={6} sm={12} lg={12}>
                                <Card>
                                    <Card.Header>
                                        <Card.Title>Profile</Card.Title>
                                    </Card.Header>
                                    <Card.Body>
                                        <Form>
                                            <Form.Group label="Content Preferences">
                                                <Form.SelectGroup
                                                    canSelectMultiple
                                                    pills
                                                >
                                                    <Form.SelectGroupItem
                                                        label="Regions"
                                                        name="language"
                                                        value="Regions"

                                                    />
                                                    <Form.SelectGroupItem
                                                        label="Thematic"
                                                        name="language"
                                                        value="Thematic"
                                                    />
                                                    <Form.SelectGroupItem
                                                        label="Equity"
                                                        name="language"
                                                        value="Equity"
                                                    />
                                                    <Form.SelectGroupItem
                                                        label="Currency"
                                                        name="language"
                                                        value="Currency"
                                                    />
                                                    <Form.SelectGroupItem
                                                        label="Tenure"
                                                        name="language"
                                                        value="Tenure"
                                                    />

                                                </Form.SelectGroup>
                                            </Form.Group>
                                            <Grid.Row>
                                                <Grid.Col>
                                                    <Text color="green">Risk Profile : 4</Text>
                                                </Grid.Col>
                                            </Grid.Row>
                                            <Grid.Row>
                                                <Grid.Col>
                                                    <Text color="blue">Interests : Golf </Text>
                                                </Grid.Col>
                                            </Grid.Row>
                                            <Grid.Row>
                                                <Grid.Col>
                                                    <Text color="blue">Source : Business wealth </Text>
                                                </Grid.Col>
                                            </Grid.Row>
                                            <Grid.Row>
                                                <Grid.Col>
                                                    <Text color="blue">Public Image & Reputation : 7 </Text>
                                                </Grid.Col>
                                            </Grid.Row>
                                            <Grid.Row>
                                                <Grid.Col>
                                                    <Text color="blue">Confidentiality & Reputation : 9 </Text>
                                                </Grid.Col>
                                            </Grid.Row>
                                            <Grid.Row>
                                                <Grid.Col>
                                                    <Text color="blue">Security : 10 </Text>
                                                </Grid.Col>
                                            </Grid.Row>
                                            <Grid.Row>
                                                <Grid.Col>
                                                    <Text color="blue">Responsiveness : 5 </Text>
                                                </Grid.Col>
                                            </Grid.Row>
                                            <Grid.Row>
                                                <Grid.Col>
                                                    <Text color="blue">Level of understanding: 7 </Text>
                                                </Grid.Col>
                                            </Grid.Row>
                                            <Grid.Row>
                                                <Grid.Col>
                                                    <Text color="blue">Product Ranking: Deposit products, Other short-term
investments, Mutual Funds  </Text>
                                                </Grid.Col>
                                            </Grid.Row>



                                            <Grid.Row>
                                                <Grid.Col>
                                                    <Text color="blue">Summary</Text>
                                                </Grid.Col>
                                            </Grid.Row>

                                        </Form>
                                    </Card.Body>
                                </Card>
                            </Grid.Col>
                        </Grid.Row>
                        <Grid.Row>
                            <Grid.Col width={6} sm={12} lg={12}>
                                <Card>
                                    <Card.Header>
                                        <Card.Title>Events & News</Card.Title>
                                    </Card.Header>
                                    <Card.Body>
                                        <Grid.Row>
                                            <Grid.Col>
                                                <div>
                                                    <a className="text-inherit">F1 Singapore</a>
                                                </div>
                                                <Text.Small muted className="d-block item-except h-1x">
                                                    Free Tickets to F1 Singapore
                                                </Text.Small>
                                            </Grid.Col>
                                            <Grid.Col auto>
                                                <Dropdown>
                                                    <Dropdown.Trigger
                                                        icon="more-vertical"
                                                        toggle={false}
                                                    />
                                                </Dropdown>
                                            </Grid.Col>
                                            <hr />
                                        </Grid.Row>
                                        <Grid.Row>
                                            <Grid.Col>
                                                <div>
                                                    <a className="text-inherit">Travels</a>
                                                </div>
                                                <Text.Small muted className="d-block item-except h-1x">
                                                    Free Travel Insurance from us
                                                </Text.Small>
                                                <hr />
                                            </Grid.Col>
                                            <Grid.Col auto>
                                                <Dropdown>
                                                    <Dropdown.Trigger
                                                        icon="more-vertical"
                                                        toggle={false}
                                                    />
                                                </Dropdown>
                                            </Grid.Col>
                                            <hr />
                                        </Grid.Row>

                                    </Card.Body>
                                </Card>
                            </Grid.Col>
                        </Grid.Row>
                    </Grid.Col>
                    <Grid.Col width={6} sm={8} lg={9}>
                        <Grid.Row>
                            <Grid.Col width={6} sm={12} lg={12}>
                                <Card>
                                    <Card.Header>
                                        <Card.Title>Performance Data</Card.Title>
                                    </Card.Header>
                                    <C3Chart
                                        style={{ height: "10rem" }}
                                        data={{
                                            columns: [
                                                // each columns data
                                                [
                                                    "data1",
                                                    12,
                                                    15,
                                                    10,
                                                    12,
                                                    13,
                                                    15,
                                                    16,
                                                    18,
                                                    24,
                                                    10,
                                                    12,
                                                    5,
                                                    6,
                                                    3,
                                                    2,
                                                    2,
                                                    6,
                                                    30,
                                                    10,
                                                    10,
                                                    15,
                                                    14,
                                                    47,
                                                    65,
                                                    55,
                                                ],
                                                [
                                                    "data2",
                                                    7,
                                                    12,
                                                    5,
                                                    6,
                                                    3,
                                                    2,
                                                    2,
                                                    6,
                                                    30,
                                                    10,
                                                    10,
                                                    15,
                                                    14,
                                                    47,
                                                    65,
                                                    55,
                                                    0,
                                                    5,
                                                    1,
                                                    2,
                                                    7,
                                                    5,
                                                    6,
                                                    8,
                                                    24,
                                                ],
                                            ],
                                            type: "area", // default type of chart
                                            groups: [["data1", "data2", "data3"]],
                                            colors: {
                                                data1: colors["blue"],
                                                data1: colors["orange"],
                                            },
                                            names: {
                                                // name of each series
                                                data1: "Purchases",
                                                data2: "TEST2",
                                            },
                                        }}
                                        axis={{
                                            y: {
                                                padding: {
                                                    bottom: 0,
                                                },
                                                show: false,
                                                tick: {
                                                    outer: false,
                                                },
                                            },
                                            x: {
                                                padding: {
                                                    left: 0,
                                                    right: 0,
                                                },
                                                show: false,
                                            },
                                        }}
                                        legend={{
                                            position: "inset",
                                            padding: 0,
                                            inset: {
                                                anchor: "top-left",
                                                x: 20,
                                                y: 8,
                                                step: 10,
                                            },
                                        }}
                                        tooltip={{
                                            format: {
                                                title: function (x) {
                                                    return "";
                                                },
                                            },
                                        }}
                                        padding={{
                                            bottom: 0,
                                            left: -1,
                                            right: -1,
                                        }}
                                        point={{
                                            show: false,
                                        }}
                                    />
                                    <Table
                                        cards={true}
                                        striped={true}
                                        responsive={true}
                                        className="table-vcenter"
                                    >
                                        <Table.Header>
                                            <Table.Row>
                                                <Table.ColHeader colSpan={2}>User</Table.ColHeader>
                                                <Table.ColHeader>Commit</Table.ColHeader>
                                                <Table.ColHeader>Date</Table.ColHeader>
                                                <Table.ColHeader />
                                            </Table.Row>
                                        </Table.Header>
                                        <Table.Body>
                                            <Table.Row>
                                                <Table.Col className="w-1">
                                                    <Avatar imageURL="./demo/faces/male/9.jpg" />
                                                </Table.Col>
                                                <Table.Col>Ronald Bradley</Table.Col>
                                                <Table.Col>Initial commit</Table.Col>
                                                <Table.Col className="text-nowrap">May 6, 2018</Table.Col>
                                                <Table.Col className="w-1">
                                                    <Icon link={true} name="trash" />
                                                </Table.Col>
                                            </Table.Row>
                                            <Table.Row>
                                                <Table.Col>
                                                    <Avatar>BM</Avatar>
                                                </Table.Col>
                                                <Table.Col>Russell Gibson</Table.Col>
                                                <Table.Col>Main structure</Table.Col>
                                                <Table.Col className="text-nowrap">
                                                    April 22, 2018
                                        </Table.Col>
                                                <Table.Col>
                                                    <Icon link={true} name="trash" />
                                                </Table.Col>
                                            </Table.Row>
                                        </Table.Body>
                                    </Table>
                                </Card>
                            </Grid.Col>
                        </Grid.Row>
                        <Grid.Row>
                            <Grid.Col width={6} lg={12}>
                                <Card>
                                    <Card.Header>
                                        <Card.Title>360 View</Card.Title>
                                    </Card.Header>
                                    <Card.Body>
                                        <Grid.Row>
                                            <Grid.Col width={6} sm={12} lg={8}>
                                                <Card>
                                                    <Tabs
                                                        scrollable
                                                        scrollButtons="on"
                                                        indicatorColor="primary"
                                                        textColor="primary"
                                                    >
                                                        <Tab label="Item One" icon={<PhoneIcon />} />
                                                        <Tab label="Item Two" icon={<FavoriteIcon />} />
                                                        <Tab label="Item Three" icon={<PersonPinIcon />} />
                                                        <Tab label="Item Four" icon={<HelpIcon />} />
                                                        <Tab label="Item Five" icon={<ShoppingBasket />} />
                                                        <Tab label="Item Six" icon={<ThumbDown />} />
                                                        <Tab label="Item Seven" icon={<ThumbUp />} />
                                                    </Tabs>
                                                    <Grid.Col sm={6} lg={12}>
                                                        <StampCard
                                                            color="blue"
                                                            icon="dollar-sign"
                                                            header={
                                                                <a>
                                                                    132 <small>Sales</small>
                                                                </a>
                                                            }
                                                            footer={"12 waiting payments"}
                                                        />
                                                    </Grid.Col>
                                                    <Grid.Col sm={6} lg={12}>
                                                        <StampCard
                                                            color="green"
                                                            icon="shopping-cart"
                                                            header={
                                                                <a>
                                                                    78 <small>Orders</small>
                                                                </a>
                                                            }
                                                            footer={"32 shipped"}
                                                        />
                                                    </Grid.Col>
                                                    <Grid.Col sm={6} lg={12}>
                                                        <StampCard
                                                            color="red"
                                                            icon="users"
                                                            header={
                                                                <a>
                                                                    1,352 <small>Members</small>
                                                                </a>
                                                            }
                                                            footer={"163 registered today"}
                                                        />
                                                    </Grid.Col>

                                                </Card>
                                            </Grid.Col>
                                            <Grid.Col width={6} sm={6} lg={4}>
                                                <Card>
                                                    <Card.Header>
                                                        <Card.Title>Portfolio</Card.Title>
                                                    </Card.Header>
                                                    <Card.Body>
                                                        <C3Chart
                                                            style={{ height: "12rem" }}
                                                            data={{
                                                                columns: [
                                                                    // each columns data
                                                                    ["data1", 63],
                                                                    ["data2", 44],
                                                                    ["data3", 12],
                                                                    ["data4", 14],
                                                                ],
                                                                type: "pie", // default type of chart
                                                                colors: {
                                                                    data1: colors["blue-darker"],
                                                                    data2: colors["blue"],
                                                                    data3: colors["blue-light"],
                                                                    data4: colors["blue-lighter"],
                                                                },
                                                                names: {
                                                                    // name of each serie
                                                                    data1: "A",
                                                                    data2: "B",
                                                                    data3: "C",
                                                                    data4: "D",
                                                                },
                                                            }}
                                                            legend={{
                                                                show: false, //hide legend
                                                            }}
                                                            padding={{
                                                                bottom: 0,
                                                                top: 0,
                                                            }}
                                                        />
                                                    </Card.Body>
                                                </Card>
                                            </Grid.Col>
                                        </Grid.Row>

                                    </Card.Body>
                                </Card>
                            </Grid.Col>
                        </Grid.Row>
                        <Grid.Row>
                            <Grid.Col width={6} sm={12} lg={12}>
                                <Card>
                                    <Card.Header>
                                        <Card.Title>Recomendations</Card.Title>
                                    </Card.Header>
                                    <Card.Body>
                                        <Table
                                            responsive
                                            highlightRowOnHover
                                            hasOutline
                                            verticalAlign="center"
                                            cards
                                            className="text-nowrap"
                                        >
                                            <Table.Header>
                                                <Table.Row>

                                                    <Table.ColHeader>Product</Table.ColHeader>
                                                    <Table.ColHeader>Usage</Table.ColHeader>
                                                    <Table.ColHeader alignContent="center">
                                                        Posted Since
                                                    </Table.ColHeader>
                                                    <Table.ColHeader alignContent="center">
                                                        Suitability
                                                    </Table.ColHeader>
                                                    <Table.ColHeader>Activity</Table.ColHeader>
                                                    <Table.ColHeader alignContent="center">
                                                        <i className="icon-settings" />
                                                    </Table.ColHeader>
                                                </Table.Row>
                                            </Table.Header>
                                            <Table.Body>
                                                <Table.Row>

                                                    <Table.Col>
                                                        <div>AAPL</div>
                                                        <Text size="sm" muted>
                                                            Apple Inc.
                                                        </Text>
                                                    </Table.Col>
                                                    <Table.Col>
                                                        <div className="clearfix">
                                                            <div className="float-left">
                                                                <strong>65%</strong>
                                                            </div>
                                                            <div className="float-right">
                                                                <Text.Small muted>
                                                                    Jun 11, 2015 - Sep 28, 2018
                                                                </Text.Small>
                                                            </div>
                                                        </div>
                                                        <Progress size="xs">
                                                            <Progress.Bar color="yellow" width={65} />
                                                        </Progress>
                                                    </Table.Col>

                                                    <Table.Col>
                                                        <div>4 minutes ago</div>
                                                    </Table.Col>
                                                    <Table.Col alignContent="center">82%</Table.Col>
                                                    <Table.Col alignContent="center">
                                                        <Dropdown>
                                                            <Dropdown.Trigger icon="more-vertical" toggle={false} />
                                                        </Dropdown>
                                                    </Table.Col>
                                                </Table.Row>

                                                <Table.Row>

                                                    <Table.Col>
                                                        <div>GOOGL</div>
                                                        <Text size="sm" muted>
                                                            Alphabet Inc Class A
                                                        </Text>
                                                    </Table.Col>
                                                    <Table.Col>
                                                        <div className="clearfix">
                                                            <div className="float-left">
                                                                <strong>75%</strong>
                                                            </div>
                                                            <div className="float-right">
                                                                <Text.Small muted>
                                                                    Jun 11, 2015 - Sep 28, 2018
                                                                </Text.Small>
                                                            </div>
                                                        </div>
                                                        <Progress size="xs">
                                                            <Progress.Bar color="green" width={75} />
                                                        </Progress>
                                                    </Table.Col>

                                                    <Table.Col>
                                                        <div>4 minutes ago</div>
                                                    </Table.Col>
                                                    <Table.Col alignContent="center">65%</Table.Col>
                                                    <Table.Col alignContent="center">
                                                        <Dropdown>
                                                            <Dropdown.Trigger icon="more-vertical" toggle={false} />
                                                        </Dropdown>
                                                    </Table.Col>
                                                </Table.Row>
                                            </Table.Body>
                                        </Table>

                                    </Card.Body>
                                </Card>
                            </Grid.Col>
                        </Grid.Row>
                        <Grid.Row>
                            <Grid.Col width={6} sm={12} lg={12}>
                                <Card>
                                    <Card.Header>
                                        <Card.Title>Opportunities</Card.Title>
                                    </Card.Header>
                                    <Card.Body>
                                        <Table
                                            responsive
                                            highlightRowOnHover
                                            hasOutline
                                            verticalAlign="center"
                                            cards
                                            className="text-nowrap"
                                        >
                                            <Table.Header>
                                                <Table.Row>

                                                    <Table.ColHeader>Product</Table.ColHeader>
                                                    <Table.ColHeader alignContent="center">
                                                        Suitability
                                                    </Table.ColHeader>
                                                    <Table.ColHeader>Activity</Table.ColHeader>
                                                    <Table.ColHeader alignContent="center">
                                                        <i className="icon-settings" />
                                                    </Table.ColHeader>
                                                </Table.Row>
                                            </Table.Header>
                                            <Table.Body>
                                                <Table.Row>

                                                    <Table.Col>
                                                        <div>ETL</div>
                                                        <Text size="sm" muted>
                                                            xxxx
                                                        </Text>
                                                    </Table.Col>
                                                    <Table.Col>
                                                        <div className="clearfix">
                                                            <div className="float-left">
                                                                <strong>65%</strong>
                                                            </div>
                                                            {/* <div className="float-right">
                                                                <Text.Small muted>
                                                                    Jun 11, 2015 - Sep 28, 2018
                                                                </Text.Small>
                                                            </div> */}
                                                        </div>
                                                        <Progress size="xs">
                                                            <Progress.Bar color="yellow" width={65} />
                                                        </Progress>
                                                    </Table.Col>
                                                    <Table.Col alignContent="center">
                                                        <Dropdown>
                                                            <Dropdown.Trigger icon="more-vertical" toggle={false} />
                                                        </Dropdown>
                                                    </Table.Col>
                                                </Table.Row>


                                            </Table.Body>
                                        </Table>

                                    </Card.Body>
                                </Card>
                            </Grid.Col>
                        </Grid.Row>
                    </Grid.Col>

                </Grid.Row>
            </Page.Content >
        </SiteWrapper >
    );
}

export default ClientView;
