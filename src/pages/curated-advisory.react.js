// @flow

import * as React from "react";

import {
    Page,
    Avatar,
    Icon,
    Grid,
    Card,
    Text,
    Table,
    Alert,
    Progress,
    colors,
    Dropdown,
    Button,
    StampCard,
    StatsCard,
    ProgressCard,
    Badge,
    Profile,
    Media,
    Form, GalleryCard,
} from "tabler-react";

import { withStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import PhoneIcon from '@material-ui/icons/Phone';
import FavoriteIcon from '@material-ui/icons/Favorite';
import PersonPinIcon from '@material-ui/icons/PersonPin';
import HelpIcon from '@material-ui/icons/Help';
import ShoppingBasket from '@material-ui/icons/ShoppingBasket';
import ThumbDown from '@material-ui/icons/ThumbDown';
import ThumbUp from '@material-ui/icons/ThumbUp';
import Typography from '@material-ui/core/Typography';

import C3Chart from "react-c3js";

import SiteWrapper from "../SiteWrapper.react";

function CuratedAdvisory() {


    return (
        <SiteWrapper>
            <Page.Content title="Curated Advisory">
                <Grid.Row>
                    <Grid.Col>
                        <Alert type="secondary">
                            <Alert.Link
                                href={
                                    process.env.NODE_ENV === "production"
                                        ? "https://tabler.github.io/tabler-react/documentation"
                                        : "/documentation"
                                }
                            >
                                Alerts
                         </Alert.Link>{" "}
                        </Alert>
                    </Grid.Col>
                </Grid.Row>
                <Grid.Row cards={true}>

                    <Grid.Col width={6} sm={4} lg={3}>
                        <Grid.Row>
                            <Grid.Col lg={12}>
                                <Profile
                                    name="Peter Richards"
                                    backgroundURL="demo/photos/eberhard-grossgasteiger-311213-500.jpg"
                                    avatarURL="demo/faces/male/16.jpg"
                                    twitterURL="test"
                                >
                                    ABC co. Ltd. CEO
                            <Media>
                                        <Media.BodySocial
                                            name=""
                                            workTitle=""
                                            facebook="Facebook"
                                            twitter="Twitter"
                                            phone="1234567890"
                                            skype="@skypename"
                                        />
                                    </Media>
                                </Profile>
                            </Grid.Col>
                        </Grid.Row>
                        <Grid.Row>
                            <Grid.Col width={6} sm={12} lg={12}>
                                <Card>
                                    <Card.Header>
                                        <Card.Title>Events & News</Card.Title>
                                    </Card.Header>
                                    <Card.Body>
                                        <Grid.Row>
                                            <Grid.Col>
                                                <div>
                                                    <a className="text-inherit">F1 Singapore</a>
                                                </div>
                                                <Text.Small muted className="d-block item-except h-1x">
                                                    Free Tickets to F1 Singapore
                                                </Text.Small>
                                            </Grid.Col>
                                            <Grid.Col auto>
                                                <Dropdown>
                                                    <Dropdown.Trigger
                                                        icon="more-vertical"
                                                        toggle={false}
                                                    />
                                                </Dropdown>
                                            </Grid.Col>
                                            <hr />
                                        </Grid.Row>
                                        <Grid.Row>
                                            <Grid.Col>
                                                <div>
                                                    <a className="text-inherit">Travels</a>
                                                </div>
                                                <Text.Small muted className="d-block item-except h-1x">
                                                    Free Travel Insurance from us
                                                </Text.Small>
                                                <hr />
                                            </Grid.Col>
                                            <Grid.Col auto>
                                                <Dropdown>
                                                    <Dropdown.Trigger
                                                        icon="more-vertical"
                                                        toggle={false}
                                                    />
                                                </Dropdown>
                                            </Grid.Col>
                                            <hr />
                                        </Grid.Row>

                                    </Card.Body>
                                </Card>
                            </Grid.Col>
                        </Grid.Row>
                    </Grid.Col>
                    <Grid.Col width={6} sm={8} lg={9}>
                        <Grid.Row>
                            <Grid.Col width={6} lg={12}>
                                <Card>
                                    <Card.Header>
                                        <Card.Title>Legal Services</Card.Title>
                                    </Card.Header>
                                    <Card.Body>
                                        <Grid.Row>
                                            <Grid.Col width={6} sm={12} lg={6}>
                                                <GalleryCard>
                                                    <GalleryCard.Image
                                                        src="../demo/photos/legal.JPG"
                                                        alt={`Photo by `}
                                                    />
                                                    <GalleryCard.Footer>
                                                        <GalleryCard.Details
                                                            avatarURL={'https://tabler.github.io/tabler/demo/faces/male/41.jpg'}
                                                            fullName={'Nathan Guerrero'}
                                                            dateString={'12 days ago'}
                                                        />
                                                        <GalleryCard.IconGroup>
                                                            <GalleryCard.IconItem name="eye" label={920} />
                                                            <GalleryCard.IconItem
                                                                name="heart"
                                                                label={2300}
                                                                right
                                                            />
                                                        </GalleryCard.IconGroup>
                                                    </GalleryCard.Footer>
                                                </GalleryCard>
                                            </Grid.Col>
                                            <Grid.Col width={6} sm={12} lg={6}>
                                                <GalleryCard>
                                                    <GalleryCard.Image
                                                        src="http://www.nssga.org/wp-content/uploads/2015/11/shutterstock_217065631.jpg"
                                                        alt={`Photo by `}
                                                    />
                                                    <GalleryCard.Footer>
                                                        <GalleryCard.Details
                                                            avatarURL={'https://tabler.github.io/tabler/demo/faces/female/1.jpg'}
                                                            fullName={'Alice Mason'}
                                                            dateString={'12 days ago'}
                                                        />
                                                        <GalleryCard.IconGroup>
                                                            <GalleryCard.IconItem name="eye" label={10120} />
                                                            <GalleryCard.IconItem
                                                                name="heart"
                                                                label={1400}
                                                                right
                                                            />
                                                        </GalleryCard.IconGroup>
                                                    </GalleryCard.Footer>
                                                </GalleryCard>
                                            </Grid.Col>
                                            <Grid.Col width={6} sm={12} lg={6}>
                                                <GalleryCard>
                                                    <GalleryCard.Image
                                                        src="https://www.terrameridiana.com/blog/wp-content/uploads/2015/11/New-law-on-bank-guarantees.jpg"
                                                        alt={`Photo by `}
                                                    />
                                                    <GalleryCard.Footer>
                                                        <GalleryCard.Details
                                                            avatarURL={'https://tabler.github.io/tabler/demo/photos/jeremy-bishop-330225-500.jpg'}
                                                            fullName={'Peter Richards'}
                                                            dateString={'2 days ago'}
                                                        />
                                                        <GalleryCard.IconGroup>
                                                            <GalleryCard.IconItem name="eye" label={780} />
                                                            <GalleryCard.IconItem
                                                                name="heart"
                                                                label={800}
                                                                right
                                                            />
                                                        </GalleryCard.IconGroup>
                                                    </GalleryCard.Footer>
                                                </GalleryCard>
                                            </Grid.Col>
                                            <Grid.Col width={6} sm={12} lg={6}>
                                                <GalleryCard>
                                                    <GalleryCard.Image
                                                        src="http://www.yourdictionary.com/image/articles/5090.Legal.jpg"
                                                        alt={`Photo by `}
                                                    />
                                                    <GalleryCard.Footer>
                                                        <GalleryCard.Details
                                                            avatarURL={'https://tabler.github.io/tabler/demo/faces/male/26.jpg'}
                                                            fullName={'Wayne Holland'}
                                                            dateString={'4 days ago'}
                                                        />
                                                        <GalleryCard.IconGroup>
                                                            <GalleryCard.IconItem name="eye" label={1100} />
                                                            <GalleryCard.IconItem
                                                                name="heart"
                                                                label={600}
                                                                right
                                                            />
                                                        </GalleryCard.IconGroup>
                                                    </GalleryCard.Footer>
                                                </GalleryCard>
                                            </Grid.Col>
                                        </Grid.Row>

                                    </Card.Body>
                                </Card>
                            </Grid.Col>
                        </Grid.Row>
                        <Grid.Row>
                            <Grid.Col width={6} lg={12}>
                                <Card>
                                    <Card.Header>
                                        <Card.Title>Reviews</Card.Title>
                                    </Card.Header>
                                    <Card.Body>
                                        <Grid.Row>
                                            <Grid.Col width={6} sm={12} lg={12}>
                                                <Card>

                                                </Card>
                                            </Grid.Col>
                                        </Grid.Row>

                                    </Card.Body>
                                </Card>
                            </Grid.Col>
                        </Grid.Row>
                    </Grid.Col>

                </Grid.Row>
            </Page.Content >
        </SiteWrapper >
    );
}

export default CuratedAdvisory;
